package com.example.demolivedataviewmodel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.demolivedataviewmodel.databinding.FragmentControlBinding

class ControlFragment: Fragment(){
    private var _binding: FragmentControlBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       // val model: MyViewModel by viewModels() //-- this way we can't access the shared viewmodel instance

        val model:  MyViewModel by activityViewModels()
        _binding = FragmentControlBinding.inflate(inflater, container, false)
        val v = binding.root


        binding.startstop.setOnClickListener{
            model.startstop()
        }

        binding.lap.setOnClickListener{
            model.slowLapCapture()
        }

      binding.cancel.setOnClickListener{
            model.fastCancel()
        }

      binding.clear.setOnClickListener{
          model.slowClear()
        }

        return v
    }

}