package com.example.demolivedataviewmodel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.demolivedataviewmodel.databinding.FragmentControlBinding
import com.example.demolivedataviewmodel.databinding.FragmentDisplayBinding

class DisplayFragment: Fragment() {

    private var _binding: FragmentDisplayBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDisplayBinding.inflate(inflater, container, false)
        val v = binding.root
//        var temp = activity

//        if(temp!=null){
//
//          val model =  ViewModelProviders.of(temp).get(MyViewModel::class.java)
//        } //old way of accessing a shared viewmodel


        val model: MyViewModel by activityViewModels() //new way of accessing a shared viewmodel instance


        model.getElapsedTime().observe(this, Observer<Long>{ time ->

            binding.timer.text = "$time seconds elapsed"

        })


        model?.getLapTime()?.observe(this, { laps ->

            binding.laps.text = laps

        })


        return v
    }
}