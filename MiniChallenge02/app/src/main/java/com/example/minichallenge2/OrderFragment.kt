package com.example.minichallenge2


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.minichallenge2.databinding.FragmentOrderBinding


class OrderFragment : Fragment(),View.OnClickListener {


    private lateinit var model: MyViewModel



    private lateinit var orangeCountTextView: TextView
    private lateinit var appleCountTextView: TextView
    private lateinit var walletTextView: TextView
    private lateinit var viewF: View
    private var apples = 0
    private var oranges = 0
    private var _binding: FragmentOrderBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentOrderBinding.inflate(inflater, container, false)
        val v = binding.root


        setUpButtonUpListeners()

        oranges=0
        apples=0
        orangeCountTextView = binding.orangeCount
        appleCountTextView = binding.appleCount
        walletTextView = binding.wallet

        val model: MyViewModel by activityViewModels()

        walletTextView.text = resources.getString(R.string.wallet)+model.cashInWallet.toString()
        return v
    }

    // the entire fragment is registered as a listener for the buttons. Notice the 'when' block (read more about it here: (https://kotlinlang.org/docs/reference/control-flow.html#when-expression). This is
    // a shorter way of 'switch-case'
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.applePlus -> apples++
            R.id.appleMinus -> if(apples>0) apples--
            R.id.orangePlus -> oranges++
            R.id.orangeMinus -> if(oranges>0) oranges--
            R.id.order -> callNavigation(v)
        }
        appleCountTextView.text = apples.toString()
        orangeCountTextView.text = oranges.toString()
    }

    private fun callNavigation(v: View?) {


            val cost  = apples*MyViewModel.apple_price+oranges*MyViewModel.orange_price


        v?.findNavController()?.navigate(R.id.action_orderFragment_to_confirmFragment, bundleOf("cost" to cost))

    }



    // registering all of the buttons with a single listener.
    private fun setUpButtonUpListeners(){
        binding.applePlus.setOnClickListener(this)
        binding.appleMinus.setOnClickListener(this)
        binding.orangePlus.setOnClickListener(this)
        binding.orangeMinus.setOnClickListener(this)
        binding.order.setOnClickListener(this)
    }
}
