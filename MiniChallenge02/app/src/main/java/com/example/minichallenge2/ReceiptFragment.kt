package com.example.minichallenge2


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import com.example.minichallenge2.databinding.FragmentOrderBinding
import com.example.minichallenge2.databinding.FragmentReceiptBinding


/**
 * A simple [Fragment] subclass.
 *
 */
class ReceiptFragment : Fragment() {

    private var _binding: FragmentReceiptBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentReceiptBinding.inflate(inflater, container, false)
        val v = binding.root
        val model: MyViewModel by activityViewModels()

        binding.receipt.text = getString(R.string.order_total)+model.orderTotal+"\n"+getString(R.string.balance_remaining)+model.cashInWallet
        model.finishTransaction()
        return v
    }


}
